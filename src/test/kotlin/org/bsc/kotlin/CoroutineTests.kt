package org.bsc.kotlin

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.jvm.*


fun add(num1: Int, num2: Int) = num1 + num2

suspend fun addWithDelay( a:Int, b:Int, delayInMills:Long = 1000 ): Int {

    delay(delayInMills)

    return a + b
}

@ExtendWith(TimingExtension::class)
internal class CoroutineTests {

    @Test
    fun `simple add test`() {

        val result = add( 1, 2)

        assertEquals( 3, result )

    }

    @Test
    fun `simple coroutine add test with 1s delay`() = runBlocking {

        val result = addWithDelay( 1, 4, 1000 )

        assertEquals( 5, result )
    }

}